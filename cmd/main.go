package main

import (
	"log"

	_ "gitlab.com/iwaseyusuke/go-web-app/pkg"

	"github.com/GoogleCloudPlatform/functions-framework-go/funcframework"
)

func main() {
	port := "8080"
	if err := funcframework.Start(port); err != nil {
		log.Fatalf("Unable to start server: %v\n", err)
	}
}
