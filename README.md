# go-web-app

A sample web application written in Go for [Tanzu Application Platform](https://docs.vmware.com/en/VMware-Tanzu-Application-Platform/index.html).

## Prerequisites

* Installed the `tanzu` CLI
* Installed the Tanzu Application Platform
* Set up a developer namespace

## How to deploy

Using `config/workload.yaml`, simply deploy it using:

```sh
tanzu apps workload apply -f config/workload.yaml -n DEVELOPER-NAMESPACE
```

## Troubleshooting

In case the version(s) of Go Buildpack is not compatible with the version specified in `config/workload.yaml`, please update the following accordingly.

```yaml
# config/workload.yaml

...(snip)...

  build:
    env:
      - name: BP_GO_VERSION
        value: 1.19

...(snip)...
```